package com.sap.francisco.arias.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Class holding information on a person.
 */
@Entity
@Table(name = "HTTP_LOG")
@NamedQuery(name = "AllHttpLogs", query = "select p from http_log p order by p.id desc")
public class http_log {
    @Id
    @GeneratedValue
    private Long id;
    @Basic
    private String method;
    @Basic
    @Lob 
    @Column(length=5000)
    private String headers;
    @Lob 
    @Column(length=5000)
    private String body;
    @Lob 
    @Column(length=5000)
    private String parameters;

    public long getId() {
        return id;
    }

    public void setId(long newId) {
        this.id = newId;
    }

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getHeaders() {
		return headers;
	}

	public void setHeaders(String headers) {
		this.headers = headers;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getParameters() {
		return parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}
}