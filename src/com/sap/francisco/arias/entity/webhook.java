package com.sap.francisco.arias.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Class holding information on a person.
 */
@Entity
@Table(name = "WEBHOOK")
@NamedQuery(name = "AllWebhooks", query = "select p from webhook p order by p.timestamp desc")
public class webhook {
    @Id
    @GeneratedValue
    private Long id;
    @Basic
    private String nonce;
    @Basic
    private int status = 0;
    @Basic
    private Long timestamp;
    @Basic
    private String type;
    @Basic
    private String uid;
    @Basic
    private String wid;

    public long getId() {
        return id;
    }

    public void setId(long newId) {
        this.id = newId;
    }

	public String getNonce() {
		return nonce;
	}

	public void setNonce(String nonce) {
		this.nonce = nonce;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getWid() {
		return wid;
	}

	public void setWid(String wid) {
		this.wid = wid;
	}

	
}