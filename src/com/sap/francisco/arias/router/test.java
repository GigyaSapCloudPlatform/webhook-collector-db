package com.sap.francisco.arias.router;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gigya.socialize.GSKeyNotFoundException;
import com.gigya.socialize.GSObject;
import com.gigya.socialize.GSRequest;
import com.gigya.socialize.GSResponse;

/**
 * Servlet implementation class test
 */
@WebServlet("/test")
public class test extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public test() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().println("TEST");
		
		// GIGYA
        // ====================================================
		
		//response.getWriter().println("APIKEY: " + com.sap.francisco.arias.config.gigya.apikey());
		//response.getWriter().println("Userkey: " + com.sap.francisco.arias.config.gigya.userkey());

    	// Step 1 - Defining the request
    	GSRequest gigya_request = new GSRequest(
    			com.sap.francisco.arias.config.gigya.apikey(), 
    			com.sap.francisco.arias.config.gigya.keysecret(), 
    			"accounts.getAccountInfo", 
    			new GSObject(),
    			true, com.sap.francisco.arias.config.gigya.userkey()
    			);
    
    	gigya_request.setUseHTTPS(true);
    	gigya_request.setAPIDomain("eu1.gigya.com");
    	
    	// Step 2 - Adding parameters
    	gigya_request.setParam("uid", "17314538c6144097a64563cabd5207f8");
        
        // Step 3 - Sending the request
        GSResponse gigya_response = gigya_request.send();
        
        String name = "-";
        
        response.getWriter().println("ERROR CODE: " + gigya_response.getErrorCode());
        // Step 4 - handling the request's response.
        if(gigya_response.getErrorCode()==0)
        {   // SUCCESS! response status = OK  
        	
        	GSObject profile_object = new GSObject();
        	profile_object = gigya_response.getObject("profile", profile_object);
        	
        	name = profile_object.getString("firstName", "") + " " + profile_object.getString("lastName", "");
        }
        
        response.getWriter().println("NAME:" + name);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
