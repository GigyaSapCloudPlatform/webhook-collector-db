package com.sap.francisco.arias.router;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Base64;

import com.gigya.socialize.*;
import com.sap.francisco.arias.entity.http_log;
import com.sap.francisco.arias.entity.webhook;
import com.sap.security.core.server.csi.IXSSEncoder;
import com.sap.security.core.server.csi.XSSEncoder;

/**
 * Servlet implementation class GigyaWebHookCollector
 */
@WebServlet("/")
public class GigyaWebHookCollectorDB extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(GigyaWebHookCollectorDB.class);

	private DataSource ds;
	private EntityManagerFactory emf;

	// =============================================================
	// INIT
	// =============================================================
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GigyaWebHookCollectorDB() {
		super();
	}

	/** {@inheritDoc} */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void init() throws ServletException {
		Connection connection = null;
		try {
			InitialContext ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");

			Map properties = new HashMap();
			properties.put(PersistenceUnitProperties.NON_JTA_DATASOURCE, ds);
			emf = Persistence.createEntityManagerFactory("GigyaWebHookCollectorDB", properties);
		} catch (NamingException e) {
			throw new ServletException(e);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void destroy() {
		emf.close();
	}

	// =============================================================
    // GET
    // =============================================================
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		response.getWriter().append("<h1>Gigya WebHook Collector DB 4/HANA from SAP Cloud Platform</h1>");
		
		try {
			
			EntityManager em = emf.createEntityManager();
			
			@SuppressWarnings("unchecked")
            List<webhook> resultList = em.createNamedQuery("AllWebhooks").getResultList();
            response.getWriter().println(
                    "<p><table border=\"1\"><tr><th colspan=\"5\">"
                            + (resultList.isEmpty() ? "" : resultList.size() + " ")
                            + "Entries in the Database</th></tr>");
            if (resultList.isEmpty()) {
                response.getWriter().println("<tr><td colspan=\"5\">Database is empty</td></tr>");
            } else {
                response.getWriter().println("<tr><th>ID</th><th>Date</th><th>Type</th><th>UID</th><th>WID</th></tr>");
            }
            IXSSEncoder xssEncoder = XSSEncoder.getInstance();
            for (webhook p : resultList) {
            	
            	Timestamp stamp = new Timestamp(p.getTimestamp() * 1000);
            	Date date = new Date(stamp.getTime());
            	
                response.getWriter().println(
                       "<tr>"
            		   + "<td>" + p.getId() + "</td>"
            		   + "<td>" + date + "</td>"
            		   + "<td>" + xssEncoder.encodeHTML(p.getType()) + "</td>"
            		   + "<td>" + xssEncoder.encodeHTML(p.getUid()) + "</td>"
            	       + "<td>" + xssEncoder.encodeHTML(p.getWid()) + "</td>"
                       + "</tr>");
            }
            
            response.getWriter().println("</table></p>");
            
		} catch (Exception e) {
			response.getWriter().println("Persistence operation failed with reason: " + e.getMessage());
			LOGGER.error("Persistence operation failed", e);
		}
	}
	
	
}
