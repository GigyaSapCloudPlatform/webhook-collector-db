package com.sap.francisco.arias.router;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Base64;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sap.security.core.server.csi.IXSSEncoder;
import com.sap.security.core.server.csi.XSSEncoder;

/**
 * Servlet implementation class webhook
 */
@WebServlet("/http_log")
public class http_log extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(http_log.class);

	private DataSource ds;
	private EntityManagerFactory emf;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public http_log() {
		super();
		// TODO Auto-generated constructor stub
	}

	// =============================================================
	// INIT
	// =============================================================

	/** {@inheritDoc} */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void init() throws ServletException {
		Connection connection = null;
		try {
			InitialContext ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");

			Map properties = new HashMap();
			properties.put(PersistenceUnitProperties.NON_JTA_DATASOURCE, ds);
			emf = Persistence.createEntityManagerFactory("GigyaWebHookCollectorDB", properties);
		} catch (NamingException e) {
			throw new ServletException(e);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void destroy() {
		emf.close();
	}

	// =============================================================
	// GET
	// =============================================================

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("application/json");
		
		try {
			
			EntityManager em = emf.createEntityManager();
			
			@SuppressWarnings("unchecked")
            List<com.sap.francisco.arias.entity.http_log> resultList = em.createNamedQuery("AllHttpLogs").getResultList();
            response.getWriter().print("[");
          
            IXSSEncoder xssEncoder = XSSEncoder.getInstance();
            String data = "";
            for (com.sap.francisco.arias.entity.http_log p : resultList) 
            {
             	
            	data +=
                       "{"
            		   + "\"id\":" + p.getId() + ","
            		   + "\"method\":\"" + xssEncoder.encodeHTML(p.getMethod()) + "\","
            		   + "\"headers\":\"" + xssEncoder.encodeHTML(p.getHeaders()) + "\","
            		   + "\"body\":\"" + xssEncoder.encodeHTML(p.getBody()) + "\","
            		   + "\"parameters\":\"" + xssEncoder.encodeHTML(p.getParameters()) + "\""
                       + "},";
            }
            
            if (data != null && data.length() > 0) {
            	data = data.substring(0, data.length() - 1);
            }
            
            response.getWriter().print(data);
            response.getWriter().print("]");
            
		} catch (Exception e) {
			response.getWriter().println("{\"error\": \"" + e.getMessage() +"\"}");
			LOGGER.error("Persistence operation failed", e);
		}
	}
}
