package com.sap.francisco.arias.router;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Base64;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gigya.socialize.GSObject;
import com.gigya.socialize.GSRequest;
import com.gigya.socialize.GSResponse;
import com.sap.francisco.arias.entity.http_log;
import com.sap.security.core.server.csi.IXSSEncoder;
import com.sap.security.core.server.csi.XSSEncoder;

/**
 * Servlet implementation class webhook
 */
@WebServlet("/webhook")
public class webhook extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory.getLogger(webhook.class);

	private DataSource ds;
	private EntityManagerFactory emf;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public webhook() {
		super();
		// TODO Auto-generated constructor stub
	}

	// =============================================================
	// INIT
	// =============================================================

	/** {@inheritDoc} */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void init() throws ServletException {
		Connection connection = null;
		try {
			InitialContext ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");

			Map properties = new HashMap();
			properties.put(PersistenceUnitProperties.NON_JTA_DATASOURCE, ds);
			emf = Persistence.createEntityManagerFactory("GigyaWebHookCollectorDB", properties);
		} catch (NamingException e) {
			throw new ServletException(e);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void destroy() {
		emf.close();
	}

	// =============================================================
	// GET
	// =============================================================

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("application/json");
		
		try {
			
			EntityManager em = emf.createEntityManager();
			
			@SuppressWarnings("unchecked")
            List<com.sap.francisco.arias.entity.webhook> resultList = em.createNamedQuery("AllWebhooks").getResultList();
            response.getWriter().print("[");
          
            IXSSEncoder xssEncoder = XSSEncoder.getInstance();
            String data = "";
            for (com.sap.francisco.arias.entity.webhook p : resultList) 
            {
            	// GIGYA
                // ====================================================
            	// Step 1 - Defining the request
            	GSRequest gigya_request = new GSRequest(
            			com.sap.francisco.arias.config.gigya.apikey(), 
            			com.sap.francisco.arias.config.gigya.keysecret(), 
            			"accounts.getAccountInfo", 
            			new GSObject(),
            			true, com.sap.francisco.arias.config.gigya.userkey()
            			);
            
            	gigya_request.setUseHTTPS(true);
            	gigya_request.setAPIDomain("eu1.gigya.com");
            	
            	// Step 2 - Adding parameters
            	gigya_request.setParam("uid", p.getUid());
                
                // Step 3 - Sending the request
                GSResponse gigya_response = gigya_request.send();
                
                String profile = "{}";
                
                // Step 4 - handling the request's response.
                if(gigya_response.getErrorCode()==0)
                {   // SUCCESS! response status = OK  
                	
                	GSObject profile_object = new GSObject();
                	profile_object = gigya_response.getObject("profile", profile_object);
                	
                	profile = profile_object.toString();
                }
   
            	data +=
                       "{"
            		   + "\"id\":" + p.getId() + ","
            		   + "\"date\":" + p.getTimestamp() + ","
            		   + "\"nonce\":\"" + xssEncoder.encodeHTML(p.getNonce()) + "\","
            		   + "\"type\":\"" + xssEncoder.encodeHTML(p.getType()) + "\","
            		   + "\"uid\":\"" + xssEncoder.encodeHTML(p.getUid()) + "\","
            		   + "\"wid\":\"" + xssEncoder.encodeHTML(p.getWid()) + "\","
            		   + "\"user_information\":" + profile + ""
                       + "},";
            }
            
            if (data != null && data.length() > 0) {
            	data = data.substring(0, data.length() - 1);
            }
            
            response.getWriter().print(data);
            response.getWriter().print("]");
            
		} catch (Exception e) {
			response.getWriter().println("{\"error\": \"" + e.getMessage() +"\"}");
			LOGGER.error("Persistence operation failed", e);
		}
	}

	// =============================================================
	// POST
	// =============================================================

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {

			doAdd(request, response);
			response.setStatus(200);

		} catch (Exception e) {
			response.getWriter().println("Persistence operation failed with reason: " + e.getMessage());
			LOGGER.error("Persistence operation failed", e);
		}
	}

	private void doAdd(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		// Extract name of person to be added from request
		String method = request.getMethod();
		String header = "";
		String parameter = "";
		String body = "-";

		String gigya_sig = "";

		// HEADERS
		Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			String headerValue = request.getHeader(headerName);

			header += headerName + ":" + headerValue + "\n";

			// Save the Gigya signature
			if (headerName.equals("x-gigya-sig-hmac-sha1")) {
				gigya_sig = headerValue;
			}
		}

		// BODY
		InputStream is = request.getInputStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		byte buf[] = new byte[2056];
		int letti;

		while ((letti = is.read(buf)) > 0)
			baos.write(buf, 0, letti);

		body = new String(baos.toByteArray());
		// response.getWriter().println("Body: " + body + "\n");

		// PARAMETERS
		Map<String, String[]> params = request.getParameterMap();
		for (Iterator<String> it = params.keySet().iterator(); it.hasNext();) {

			String k = it.next();
			parameter += k + ":";

			String[] v = params.get(k);
			if (v != null && v.length > 0) {
				for (int i = 0; i < v.length; i++) {
					parameter += v[i] + ",";
				}
			}

			parameter += "\n";
		}

		// Save the call
		// ================================
		EntityManager em = emf.createEntityManager();

		http_log call_http = new http_log();

		call_http.setBody(body);
		call_http.setHeaders(header);
		call_http.setMethod(method);
		call_http.setParameters(parameter);

		em.getTransaction().begin();
		em.persist(call_http);
		em.getTransaction().commit();

		// Validation
		// ================================

		byte[] secret_x64 = Base64.getDecoder().decode(com.sap.francisco.arias.config.gigya.keysecret().getBytes());

		Mac mac = Mac.getInstance("HmacSHA1");
		SecretKeySpec secret = new SecretKeySpec(secret_x64, "HmacSHA1");
		mac.init(secret);
		byte[] digest = mac.doFinal(body.getBytes());

		byte[] message_hash = Base64.getEncoder().encode(digest);
		String message_hast_sig = new String(message_hash);

		response.getWriter().println("--------------------");
		response.getWriter().println("GIGYA SIG: " + gigya_sig);
		response.getWriter().println("MESSG SIG: " + message_hast_sig);
		response.getWriter().println("--------------------");


		 if (!gigya_sig.equals(message_hast_sig)) 
		 { 
			 em.close(); 
			 throw new Exception("FRAUD"); 
		 }

		// EVENTS
		// ================================

		try {

			// WEBHOOK
			// ================================

			// Convert the string in JSON object
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(body);
			JSONObject jsonObject = (JSONObject) obj;

			// General information
			String nonce = (String) jsonObject.get("nonce");
			response.getWriter().println("Nonce: " + nonce);

			// loop event object
			JSONArray events = (JSONArray) jsonObject.get("events");
			for (Object e : events) {

				JSONObject ev = (JSONObject) e;

				// Info
				String id = (String) ev.get("id");
				String type = (String) ev.get("type");
				Long timestamp = (Long) jsonObject.get("timestamp");

				JSONObject data = (JSONObject) ev.get("data");
				String uid = (String) data.get("uid");

				// DEBUG
				response.getWriter().println("--------------------");
				response.getWriter().println("ID: " + id);
				response.getWriter().println("Type: " + type);
				response.getWriter().println("UID: " + uid);
				response.getWriter().println(timestamp.toString());

				// SAVE WEBHOOK

				com.sap.francisco.arias.entity.webhook wb_ = new com.sap.francisco.arias.entity.webhook();

				wb_.setWid(id);
				wb_.setType(type);
				wb_.setUid(uid);
				wb_.setTimestamp(timestamp);
				wb_.setNonce(nonce);

				em.getTransaction().begin();
				em.persist(wb_);
				em.getTransaction().commit();

			}

		} catch (ParseException e) {
			e.printStackTrace();
		} finally {
			em.close();
		}

	}

}
